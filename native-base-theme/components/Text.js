// @flow

import variable from './../variables/platform';

export default (variables /* : * */ = variable) => {
  const textTheme = {
    fontSize: variables.DefaultFontSize,
    fontFamily: variables.fontFamily,
    color: variables.textColor,
    '.note': {
      color: '#707070',
      fontSize: variables.noteFontSize
    },
    '.bold': {
      color: '#707070',
      fontFamily: variables.titleFontfamily,
      fontSize: variables.noteFontSize
    },
    '.title': {
      color: '#000',
      fontFamily: variables.titleFontfamily,
      // fontSize: variables.noteFontSize
    },
    '.danger': {
      color: 'red',
    },
    '.success': {
      color: 'green',
    },

  };

  return textTheme;
};
