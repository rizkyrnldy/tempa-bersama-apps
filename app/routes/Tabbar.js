import React, { Component } from 'react'
import { View } from 'react-native';
import { Footer, FooterTab, Button, Text, Icon } from 'native-base';
// import { Ionicons } from '@expo/vector-icons';

export default class Tabbar extends Component {
  render() {
    const { state, descriptors, navigation } = this.props;
    const focusedOptions = descriptors[state.routes[state.index].key].options;
    if (focusedOptions.tabBarVisible === false) {
        return null;
      }
      return (
        <Footer>
          {state.routes.map((route, index) => {
            const { options } = descriptors[route.key];
            const label = options.tabBarLabel !== undefined
                ? options.tabBarLabel
                : options.title !== undefined
                ? options.title
                : route.name;

            const isFocused = state.index === index;

            const onPress = () => {
              const event = navigation.emit({
                type: 'tabPress',
                target: route.key,
                canPreventDefault: true,
              });

              if (!isFocused && !event.defaultPrevented) {
                navigation.navigate(route.name);
              }
            };

            const onLongPress = () => {
              navigation.emit({
                type: 'tabLongPress',
                target: route.key,
              });
            };

            let iconName;
            if (route.name === 'ProjectPage') {
              iconName = isFocused ? {ic: 'history', type: 'Octicons'} : {ic: 'history', type: 'Octicons'};
            } else if (route.name === 'ScannerPage') {
              iconName = isFocused ? {ic: 'camera', type: 'AntDesign'} : {ic: 'camerao', type: 'AntDesign'};
            } else if (route.name === 'MorePage') {
              iconName = isFocused ? {ic: 'view-grid', type: 'MaterialCommunityIcons'} : {ic: 'grid', type: 'Feather'};
            }
            return (
              <FooterTab key={index}>
                <Button vertical onPress={onPress}>
                  <Icon name={iconName.ic} active type={iconName.type}style={{color: isFocused ? "#0d47a1" : "#a2a2a2", fontSize: 18, marginBottom: 2}} />
                  <Text ellipsizeMode="tail" numberOfLines={1} style={{fontFamily: 'FiraSans', color: isFocused ? "#0d47a1" : "#a2a2a2", textTransform: 'capitalize'}}>{`${label}`}</Text>
                </Button>
              </FooterTab>
            );
          })}
        </Footer>
      );
  }
}

