import React, { Component } from 'react'
import { connect } from 'react-redux'
import { checkAuth } from '../redux/actions/authAction';
import * as Containers from '../src/containers';
import { Loading } from '../src/components';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Tabbar from './Tabbar';
const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();
class Routes extends Component {
 
  componentDidMount() {
    const { actioncheckAuth } = this.props;
    return actioncheckAuth()
  }

  render() {
    const { getAuth: { loading, authed }} = this.props
    if(loading){  
      return <Loading />  
    }
    return (
      <NavigationContainer>
        <Stack.Navigator screenOptions={{ headerStyle: { backgroundColor: '#fff'}, headerTintColor: '#062b54', }}>
          { !authed ?  this.renderAuth() : this.renderPrivate() }
        </Stack.Navigator>
      </NavigationContainer>
    )
  }

  renderTabBar = () => {
    const { getAuth: { dataUser }} = this.props
    return(
      <Tab.Navigator tabBar={props => <Tabbar {...props} />}>
        <Tab.Screen name="ProjectPage" component={Containers.Project} options={{ tabBarLabel: 'Project' }}/>
        {dataUser.userType !== 6 &&
          <Tab.Screen name="ScannerPage" component={Containers.Scanner} options={{ tabBarLabel: 'Scanner' }}/> 
        }
        <Tab.Screen name="MorePage" component={Containers.More} options={{ tabBarLabel: 'Lainnya' }}/>
      </Tab.Navigator>
    )
  }

  renderPrivate(){
    return(
      <>
        <Stack.Screen name="root" component={this.renderTabBar} options={{ headerShown: false }} />
        <Stack.Screen name="ProjectDetailPage" component={Containers.ProjectDetail}  options={{ title: 'Detail Project' }} />
        <Stack.Screen name="ListProjectItem" component={Containers.ListProjectItem}  options={{ title: 'List Project Item' }} />
        <Stack.Screen name="AddSizeProject" component={Containers.AddSizeProject}  options={{ title: 'Lengkapi Ukuran' }} />
        
        <Stack.Screen name="ScannerDetailPage" component={Containers.ScannerDetail}  options={{ title: 'Detail Project' }} />
      </>
    )
  }

  renderAuth(){
    return(
      <>
        <Stack.Screen name="LoginPage" component={Containers.Login} options={{ headerShown: false }} />
      </>
    )
  }

}

const mapStateToProps = (state) => ({
  getAuth: state.auth
})

const mapDispatchToProps = {
  actioncheckAuth: checkAuth,
}

export default connect(mapStateToProps, mapDispatchToProps)(Routes)
