import { StyleSheet, Dimensions } from 'react-native';

const global = StyleSheet.create({

  container: {
    padding: 10,
    paddingLeft: 20,
    paddingRight: 20,
  },


  textCenter: {
    textAlign: 'center'
  },

  l_container: {
    paddingLeft: 15,
    paddingRight: 15,
  },

  bgWhite: {
    backgroundColor: '#fff'
  },

  centered: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },

  middle: {
    flexDirection: 'column',
    flex: 1,
    justifyContent: 'center',
  },

  justify: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },

  activeDotStyle: {
    width: 10, 
    height: 10, 
    borderRadius: 100, 
    backgroundColor:'#0d47a1', 
    // marginLeft: 10, 
    // marginRight: 10, 
    borderWidth: 1, 
    borderColor: '#707070' 
  },

  dotStyle: {
    width: 10, 
    height: 10, 
    borderRadius: 100, 
    borderWidth: 1, 
    backgroundColor:'#fff',
    borderColor: '#707070'
  }



});


module.exports = global;
