import CryptoJS from "react-native-crypto-js";
import encrypt  from 'sha1';
import { Base64 } from 'js-base64';
const signature = "TEMPA::INDONESIA";

export const encode = (val) => {
	var encrypt = CryptoJS.AES.encrypt(JSON.stringify(val), signature).toString();
	return Base64.encode(encrypt)
}

export const decode = (val) => {
	if(val){
		try {
			var bytes  = CryptoJS.AES.decrypt(Base64.decode(val), signature);
			return bytes.toString(CryptoJS.enc.Utf8);	
		} catch (error) {
			return null;
		}
	}
	return null;
}

export const generatePassword = (pass) => {
	var key = encrypt(signature + pass);
	return encrypt(key + pass);
}
