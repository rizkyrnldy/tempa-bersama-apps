// export const validationForm = (data, scb, fcb) => {
//   var result = {};
//   var err = 0
//   for (const [name, val] of Object.entries(data)) {
//     var _name = name;
//     var _value = val.value;
//     if(_value === ""){
//       result[_name] = {value: '', error: true}
//       err = 1
//     }else{
//       result[_name] = {value: _value, error: false}
//       err = 0;
//     }
//   }
//   return err === 0 ? scb(result) : fcb(result);
// }

export const validationForm = (data, scb, fcb) => {
  var array = [];
  var error = 0;
  var result = {};
  data.forEach(res => {
    var _res = res;
    if(!res.value || (res.type === 'autocomplete' && !res.value.value)){
      _res.error = true;
      error = 1;
      return array.push(_res);
    }else{
      _res.error = false;
      error = 0;
      result[res.name] = res.type === 'autocomplete' ? res.value.value : res.value
      return array.push(_res);
    }
  });
  return error === 0 ? scb(result) : fcb(array);
}