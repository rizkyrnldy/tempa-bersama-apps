import React, { Component } from 'react'
import { connect } from 'react-redux';
import { View } from 'react-native'
import { Content, Text, Button, Toast, Icon } from 'native-base';
import { Loading, NewContainer } from '../../components';
import { global } from '../../styles';
import { setDetailScanner, setSubmitDetailScanner, unmountDetailScanner } from '../../../redux/actions/scanner/scannerAction';
import { CardInformation, CardProject } from './partial';

class DetailScanner extends Component {
  constructor(props) {
    super(props)
    this.state = {
      _btnLoading: false
    }
  }

  componentDidMount() {
    const { actionsetDetailScanner } = this.props;
    const { params: { data } } = this.props.route;
    return actionsetDetailScanner({uid: JSON.parse(data)}, () => {
      return Toast.show({ text: 'Data Berhasil Didapatkan', type: "success" });
    }, (err) => {
      return Toast.show({ text: err.message, type: "danger" });
    })
  }
  
  render() {
    const { getData: { data, loading } } = this.props;
    const { _btnLoading } = this.state;
    if(loading){
      return <Loading />
    }
    return (
      <NewContainer loading={_btnLoading}>
        <Content padder>
          <CardInformation 
            passProps={this.props}
          />
          <CardProject 
            passProps={this.props}
          />
        </Content>

        {data.isScan === 0 ?
          <View style={[global.container]}>
            <Button block rounded onPress={() => this.onSubmit()} disabled={_btnLoading}>
              <Text>Kirim</Text>
            </Button>
          </View> : 
          <View style={[global.container, { backgroundColor: 'red', padding: 15}]}>
            <View style={{flexDirection: 'row', alignSelf: 'center', justifyContent: 'center'}}>
              <Icon name="alert-circle" type="Feather" style={{color: '#fff', fontSize: 16, marginRight: 5, marginTop: 2}}/>
              <Text bold style={{ color: '#fff' }}>
                ITEM INI SUDAH DISCAN
              </Text>
            </View>
          </View>
        }
      </NewContainer>
    )
  }

  onSubmit(){
    const { actionsetSubmitDetailScanner,getData: { data }, navigation } = this.props;
    var payload = { uid: data.uid }
    return this.setState({ _btnLoading: true }, () => {
      return actionsetSubmitDetailScanner(payload, () => {
        this.setState({ _btnLoading: false }, () => {
          Toast.show({ text: 'Data Berhasil Terkirim', type: "success" });
          return navigation.goBack();
        })
      }, (err) => this.setState({ _btnLoading: false }, () => {
          return Toast.show({ text: err.message, type: "danger" });
        })
      )
    })
  }

  componentWillUnmount(){
    const { actionunmountDetailScanner } = this.props;
    return actionunmountDetailScanner();
  }
}

const mapActionsToProps = {
  actionsetDetailScanner: setDetailScanner,
  actionsetSubmitDetailScanner: setSubmitDetailScanner,
  actionunmountDetailScanner: unmountDetailScanner
}

const mapStateToProps = (state, props) => {
  return {
    getData: state.scanner,
  }
}

export default connect( mapStateToProps, mapActionsToProps)(DetailScanner);