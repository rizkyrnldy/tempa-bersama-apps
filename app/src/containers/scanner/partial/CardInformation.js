import React, { Component } from 'react'
import { View } from 'react-native'
import { Card, CardItem, Text, Left } from 'native-base';

export default class CardInformation extends Component {
  render() {
    const { getData: { data } } = this.props.passProps;
    return (
      <Card>
        <CardItem>
          <Left>
            <View style={{flexDirection: 'column'}}>
              <Text bold style={{alignSelf: 'flex-start'}}>Nama Pemilik:</Text>
              <Text note style={{alignSelf: 'flex-start'}}>{data.description.NAMA}</Text>
            </View>
          </Left>
        </CardItem>

        <CardItem>
          <Left>
            <View style={{flexDirection: 'column'}}>
              <Text bold style={{alignSelf: 'flex-start'}}>NIP:</Text>
              <Text note style={{alignSelf: 'flex-start'}}>{data.description.NIP}</Text>
            </View>
          </Left>
        </CardItem>
        
        <CardItem>
          <Left>
            <View style={{flexDirection: 'column'}}>
              <Text bold style={{alignSelf: 'flex-start'}}>Pangkat:</Text>
              <Text note style={{alignSelf: 'flex-start'}}>{data.description.PANGKAT}</Text>
            </View>
          </Left>
        </CardItem>
        
        <CardItem>
          <Left>
            <View style={{flexDirection: 'column'}}>
              <Text bold style={{alignSelf: 'flex-start'}}>Ket.:</Text>
              <Text note style={{alignSelf: 'flex-start'}}>{data.description['KET.']}</Text>
            </View>
          </Left>
        </CardItem>

      </Card>
    )
  }
}
