import React, { Component } from 'react'
import { View, ToastAndroid } from 'react-native'
import { Container, Content,  Card, CardItem, Text, Button, H3, Badge, Left, Right, Toast } from 'native-base';
import moment from 'moment';

export default class CardProject extends Component {
  render() {
    const { getData: { data } } = this.props.passProps;
    const type = data.planProject.count === data.planProject.total ? true : false;
    return (
      <Card>
        <CardItem>
          <Left>
            <View style={{flexDirection: 'column'}}>
              <Text bold style={{alignSelf: 'flex-start'}}>Nama Project:</Text>
              <Text note style={{alignSelf: 'flex-start'}}>{data.planProject.name}</Text>
            </View>
          </Left>
        </CardItem>
        
        <CardItem>
          <Left>
            <View style={{flexDirection: 'column'}}>
              <Text bold style={{alignSelf: 'flex-start'}}>Deadline:</Text>
              <Text note style={{alignSelf: 'flex-start'}}>{moment(data.planProject.deadline).format('DD MMMM YYYY')}</Text>
            </View>
          </Left>
        </CardItem>
        
        <CardItem>
          <Left>
            <View style={{flexDirection: 'column'}}>
              <Text bold>Total Item:</Text>
              <View style={{flexDirection: 'row'}}>
                <Text bold style={{fontSize: 12, color: type ? 'green' : 'red'}}>{data.planProject.count}</Text>
                <Text bold style={{fontSize: 12, color: type ? 'green' : 'red'}}>/</Text>
                <Text bold style={{fontSize: 12, color: type ? 'green' : 'red'}}>{data.planProject.total}</Text>
              </View>
            </View>
          </Left>
        </CardItem>

        <CardItem>
          <Left>
            <View style={{flexDirection: 'column'}}>
              <Text bold style={{alignSelf: 'flex-start'}}>Status:</Text>
              <Text note style={{alignSelf: 'flex-start'}}>{data.planProject.isHold ? 'Sedang di Hold' : 'Sedang Berjalan'}</Text>
            </View>
          </Left>
        </CardItem>

        <CardItem>
          <Left>
            <View style={{flexDirection: 'column'}}>
              <Text bold style={{alignSelf: 'flex-start'}}>Catatan:</Text>
              <Text note style={{alignSelf: 'flex-start'}}>{data.planProject.notes}</Text>
            </View>
          </Left>
        </CardItem>
      </Card>
    )
  }
}
