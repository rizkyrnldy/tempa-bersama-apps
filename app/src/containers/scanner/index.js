import React, { Component } from 'react'
import { connect } from 'react-redux'
import { View, Modal } from 'react-native'
import { Content, Text, Button, H2, Toast } from 'native-base'
import { NewContainer } from '../../components'
import { global } from '../../styles'
import { decode } from '../../helper'
import CameraScanner from './Camera'
import * as Permissions from 'expo-permissions'

class DetailProject extends Component {
  constructor(props) {
    super(props)
    this.state = {
      _btnLoading: false,
      _modalVisible: false,
      _hasCameraPermission: null,
    }
  }
  
  render() {
    const { getAuth: { dataUser } } = this.props;
    const { _btnLoading, _modalVisible } = this.state;
    return (
      <NewContainer loading={_btnLoading}>
        <Content contentContainerStyle={{flex: 1}} padder>
          <View style={{alignSelf: 'center', justifyContent: 'center', flex: 1}}>
            <H2 style={{textAlign: 'center'}}>Hi, {dataUser.userContact.fullName}</H2>
            <Text style={{textAlign: 'center', marginVertical: 10}}>
              Silahkan melakukan scan QR code yang berada dimasing - masing item untuk merubah status progress pada proyek tersebut
            </Text>
            <View style={[global.container]}>
              <Button block rounded onPress={() => this.onOpenModal()}>
                <Text>MULAI SCAN</Text>
              </Button>
            </View>
          </View>
          <Modal
            animationType="slide"
            transparent={true}
            visible={_modalVisible}
            onRequestClose={() => this.onCloseModal()}>
              <CameraScanner 
                passState={this.state}
                passProps={this.props}
                onCloseModal={() => this.onCloseModal()}
                onSubmit={(e) => this.onSubmit(e)} 
              />
          </Modal>
        </Content>
      </NewContainer>
    )
  }

  async onOpenModal(){
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    if(status === 'granted'){
      return this.setState({
        _hasCameraPermission: status === 'granted',
        _modalVisible: true,
        _btnLoading: true
      });
    }else{
      return Toast.show({
        text: "Permission kamera tidak di izinkan",
        buttonText: "Okay",
        type: "danger"
      });
    }
  }

  onCloseModal(){
    return this.setState({ _btnLoading: false, _modalVisible: false })
  }
  
  onSubmit(e){
    const { navigation } = this.props;
    const newData = decode(e.data);
    if(newData){
      return this.setState({ _btnLoading: false, _modalVisible: false }, () => {
        return navigation.navigate('ScannerDetailPage', {
          data: newData
        });
      })
    }else{
      return this.setState({ _btnLoading: false, _modalVisible: false }, () => {
        return Toast.show({ text: "QR Code tidak valid", buttonText: "Okay", type: "danger" });
      })
    }
  }
  
}


const mapActionsToProps = {
}

const mapStateToProps = (state, props) => {
  return {
    getData: state.project.detail,
    getAuth: state.auth,
  }
}

export default connect( mapStateToProps, mapActionsToProps)(DetailProject);