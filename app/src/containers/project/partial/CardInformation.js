import React, { Component } from 'react'
import { View } from 'react-native'
import { Card, CardItem, Text, Body, Right } from 'native-base';

export default class CardInformation extends Component {
  render() {
    const { getData: { data } } = this.props.passProps;
    return (
      <Card style={{ paddingBottom: 20 }}>
        <CardItem header bordered>
          <Text>Data Informasi</Text>
        </CardItem>
        
        <CardItem style={{ paddingBottom: 0 }}>
          <Body><Text bold style={{alignSelf: 'flex-start'}}>Nama Order:</Text></Body>
          <Right><Text note>{data.name}</Text></Right>
        </CardItem>
        
        <CardItem style={{ paddingBottom: 0 }}>
          <Body><Text bold style={{alignSelf: 'flex-start'}}>Jumlah Pesanan:</Text></Body>
          <Right><Text note>{data.total}</Text></Right>
        </CardItem>

        <CardItem style={{ paddingBottom: 0 }}>
          <Body><Text bold style={{alignSelf: 'flex-start'}}>Status:</Text></Body>
          <Right><Text note>{data.isHold === 0 ? 'On Going' : 'Hold'}</Text></Right>
        </CardItem>
        
        <CardItem style={{ paddingBottom: 0 }}>
          <Body><Text bold style={{alignSelf: 'flex-start'}}>Keterangan:</Text></Body>
          <Right><Text note>{data.notes}</Text></Right>
        </CardItem>

      </Card>
    )
  }
}
