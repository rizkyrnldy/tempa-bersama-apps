import React, { Component } from 'react'
import { Card, CardItem, Text, Body, Right } from 'native-base';

export default class CardSize extends Component {
  render() {
    const { getData: { data } } = this.props.passProps;
    return (
      <Card style={{ paddingBottom: 20 }}>
        <CardItem header bordered>
          <Text>Data Jenis Ukuran</Text>
        </CardItem>
        {data.size.length > 0 && data.size.map((res, i) => (
          <CardItem style={{ paddingBottom: 0 }} key={i}>
            <Body style={{alignSelf: 'center'}}>
              <Text bold style={{alignSelf: 'flex-start', fontSize: 16}}>
                {res.typeSize}
              </Text>
            </Body>
            <Right>
              {res.value.length > 0 && res.value.map((_res, _i) => (
                <Text note key={_i}>{_res}</Text>
              ))}
            </Right>
          </CardItem>
        ))}
      </Card>
    )
  }
}
