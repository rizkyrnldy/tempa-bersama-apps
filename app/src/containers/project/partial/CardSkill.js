import React, { Component } from 'react'
import { Card, CardItem, Text, Body } from 'native-base';

export default class CardSkill extends Component {
  render() {
    const { getData: { data } } = this.props.passProps;
    return (
      <Card style={{ paddingBottom: 20 }}>
        <CardItem header bordered>
          <Text>Data Jenis Skill atau Tukang</Text>
        </CardItem>
        <CardItem style={{ paddingBottom: 0 }}>
          <Body>
            {data.skill.length > 0 && data.skill.map((res, i) => (
              <Text style={{alignSelf: 'flex-start'}} key={i}>
                {`${i+1}. ${res}`}
              </Text>
            ))}
          </Body>
        </CardItem>
      </Card>
    )
  }
}
