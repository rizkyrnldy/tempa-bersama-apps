import React, { Component } from 'react'
import { connect } from 'react-redux';
import { View, RefreshControl } from 'react-native'
import { Content, Button, Text } from 'native-base';
import { Loading, NewContainer } from '../../components';
import { global } from '../../styles';
import { setDetailProject,  unmountDetailProject } from '../../../redux/actions/project/projectAction';
import { CardInformation, CardSize, CardSkill } from './partial';

class DetailProject extends Component {
  constructor(props) {
    super(props)
    this.state = {
      _current: 1,
      _loading: false
    }
  }
  
  componentDidMount(){
    return this.getData();
  }

  getData(){
    const { actionsetDetailProject } = this.props;
    const { params: { uid } } = this.props.route;
    return actionsetDetailProject({ uid: uid });
  }
  
  render() {
    const { getData: { data, loading }, navigation } = this.props;
    const { _loading } = this.state;
    if(loading){
      return <Loading />
    }
    return (
      <NewContainer loading={_loading}>
        <Content padder refreshControl={<RefreshControl onRefresh={() => this.getData()} />}>
          <CardInformation 
            passProps={this.props}
          />
          
          <CardSize 
            passProps={this.props}
          />
          
          <CardSkill 
            passProps={this.props}
          />

        </Content>
        <View style={[global.container]}>
          <Button block rounded onPress={() => navigation.navigate('ListProjectItem', { uid: data.uid })}>
            <Text>List Item</Text>
          </Button>
        </View>
      </NewContainer>
    )
  }

  componentWillUnmount(){
    const { acctionunmountDetailProject } = this.props;
    return acctionunmountDetailProject();
  }
}

const mapActionsToProps = {
  actionsetDetailProject: setDetailProject,
  acctionunmountDetailProject: unmountDetailProject,
}

const mapStateToProps = (state, props) => {
  return {
    getData: state.project.detail,
  }
}

export default connect( mapStateToProps, mapActionsToProps)(DetailProject);