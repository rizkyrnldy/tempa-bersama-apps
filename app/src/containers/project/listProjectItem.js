import React, { Component } from 'react'
import { connect } from 'react-redux';
import { View, RefreshControl, ActivityIndicator } from 'react-native'
import { Content, Text, Icon, Header, Body, Item, Button, Card, CardItem, Input } from 'native-base';
import { Loading, NewContainer } from '../../components';
import { global } from '../../styles';
import debounce from 'lodash.debounce';
import { setListProjectItem, setLoadMoreListProjectItem, unmountListProjectItem } from '../../../redux/actions/project/projectAction';

class ListProjectItem extends Component {
  constructor(props) {
    super(props)
    this.state = {
      _current: 1,
      _filter: {
        _type: 'created_at',
        _orderBy: 'desc',
        _search: null,
      },
      _loading: false,
      _loadingSearch: false,
    }
    this.onSearch = debounce(this.onSearch.bind(this), 800);
  }
  
  componentDidMount(){
    return this.getData();
  }

  getData(){
    const { actionsetListProjectItem } = this.props;
    const { params: { uid } } = this.props.route;
    const { _current, _filter } = this.state;
    var payload = {
      uid: uid,
      current: _current,
      filter: _filter
    }
    return actionsetListProjectItem(payload, () => {
      return this.setState({ _loading: false, _loadingSearch: false })
    }, () => {
      return this.setState({ _loading: false, _loadingSearch: false })
    });
  }
  
  onRefresh(){
    return this.setState({
      _current: 1,
      _filter: {
        _type: 'created_at',
        _orderBy: 'desc',
        _search: null,
      },
      _loading: true
    }, () => this.getData())
  }

  setCurrentReadOffset = (event) => {
    const { actionsetLoadMoreListProjectItem } = this.props;
    const { params: { uid } } = this.props.route;
    const scrollHeight = event.nativeEvent.contentOffset.y + event.nativeEvent.layoutMeasurement.height;
    if(scrollHeight == event.nativeEvent.contentSize.height){
      return this.setState({ _current: this.state._current + 1, _loading: true }, () => {
        var value = { 
          uid: uid,
          current: this.state._current,
          filter: this.state._filter
        }
        return actionsetLoadMoreListProjectItem(value, () => {
          this.setState({_loading: false})
        });
      })
    }
  }

  render() {
    const { getData: { data, loading }, getAuth: { dataUser } } = this.props;
    const { _loading, _loadingSearch } = this.state;
    if(loading){
      return <Loading />
    }
    return (
      <View style={{flex: 1}}>
        <Header searchBar rounded>
          <Item>
            <Icon name="ios-search" />
            <Input placeholder="Cari Nama" onChangeText={(e) => this.onSearch(e)}/>
            {_loadingSearch && <ActivityIndicator style={[{ marginRight: 10 }]} size="small" color="#0d47a1" />}
          </Item>
        </Header>
        <NewContainer loading={_loading}>
          <Content 
            refreshControl={<RefreshControl refreshing={false} onRefresh={() => this.onRefresh()} />}
            scrollEventThrottle={300}
            onScroll={this.setCurrentReadOffset}
          >
            <View style={[global.container]}>
              {data.length > 0 && data.map((res, i) => (
                <Card key={i} >
                  <CardItem>
                    <Body>
                      <View style={{marginBottom: 10}}>
                        <Text note bold>Nama:</Text>
                        <Text note>{res.name}</Text>
                      </View>
                      <View style={[global.justify, {width: '100%'}]}>
                        <View style={{marginBottom: 10}}>
                          <Text note bold>Total Ukuran:</Text>
                          <Text note style={{fontSize: 12}}>
                            {res.totalSize}
                          </Text>
                        </View>
                        <View style={{marginBottom: 10}}>
                          <Text note bold style={{ textAlign: 'right' }}>Total Skill:</Text>
                          <Text note style={{fontSize: 12, textAlign: 'right'}}>
                            {res.totalSkill}
                          </Text>
                        </View>
                      </View>
                      <View style={[global.justify, {width: '100%'}]}>
                        <View style={{marginBottom: 10}}>
                          <Text note bold>Status Ukuran:</Text>
                          <Text note style={{fontSize: 12}} danger={res.statusSize} success={!res.statusSize}>
                            {res.statusSize ? 'Belum Lengkap' : 'Sudah Lengkap'}
                          </Text>
                        </View>
                        <View style={{marginBottom: 10}}>
                          <Text note bold style={{ textAlign: 'right' }}>Status Skill:</Text>
                          <Text note style={{fontSize: 12, textAlign: 'right'}} danger={res.statusSkill} success={!res.statusSkill}>
                            {res.statusSkill ? 'Belum Lengkap' : 'Sudah Lengkap'}
                          </Text>
                        </View>
                      </View>
                    </Body>
                  </CardItem>
                  {dataUser.userType === 6 && 
                    <CardItem bordered footer>
                      <Body>
                        <Button small full rounded onPress={() => this.goToPage(res)}>
                          <Text>LENGKAPI UKURAN</Text>
                        </Button>
                      </Body>
                    </CardItem>
                  }
                </Card>
              ))}
            </View>
          </Content>
        </NewContainer>
      </View>
    )
  }


  onSearch(e){
    return this.setState({
      _current: 1,
      _filter: {
        _type: 'created_at',
        _orderBy: 'desc',
        _search: e,
      },
      _loading: true,
      _loadingSearch: true
    }, () => this.getData())
  }

  goToPage(res){
    const { navigation } = this.props;
    return navigation.navigate('AddSizeProject', {
      uid: res.uid,
      onRefresh: () => this.getData()
    })
  }

  componentWillUnmount(){
    const { acctionunmountListProjectItem } = this.props;
    return acctionunmountListProjectItem();
  }
}

const mapActionsToProps = {
  actionsetListProjectItem: setListProjectItem,
  actionsetLoadMoreListProjectItem: setLoadMoreListProjectItem,
  acctionunmountListProjectItem: unmountListProjectItem,
}

const mapStateToProps = (state, props) => {
  return {
    getData: state.project.listItem,
    getAuth: state.auth,
  }
}

export default connect( mapStateToProps, mapActionsToProps)(ListProjectItem);