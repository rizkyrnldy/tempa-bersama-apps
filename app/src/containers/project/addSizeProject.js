import React, { Component } from 'react'
import { connect } from 'react-redux'
import { View } from 'react-native'
import { Content, Button, Card, CardItem, Text, Body, Right, Input, Item, Toast } from 'native-base'
import { Loading, NewContainer } from '../../components'
import { global } from '../../styles'
import { setListSize, setEditSize, unmountListSize } from '../../../redux/actions/project/projectAction'
import update from 'react-addons-update'

class AddSizeProject extends Component {
  constructor(props) {
    super(props)
    this.state = {
      _loading: false,
      _form: []
    }
  }

  componentDidMount(){
    const { actionsetListSize } = this.props;
    const { params: { uid } } = this.props.route;
    return actionsetListSize({ uid: uid, type: 'size' }, (res) => {
      return this.setState({ _form: res })
    });
  }

  onChange(value, indexType, keyValue){
    var _regex = /^[(]{0,1}[0-9]{1,4}[)]{0,1}[0-9]*$/g;
    // if(value.match(_regex)){
      return this.setState({
        _form: update(this.state._form, {
          [indexType]: {
            value: {
              [keyValue]: {$set: value}
            }
          }
        })
      })
    // }else{
    //   return Toast.show({ text: "Format Harus Angka", type: "danger"});
    // }
  }

  render() {
    const { getData: { loading } } = this.props;
    const { _loading, _form } = this.state;
    if(loading){
      return <Loading />
    }
    return (
      <NewContainer loading={_loading}>
        <Content padder>
            {_form.length > 0 && _form.map((res, i) => (
              <Card key={i}>
                <CardItem header bordered>
                  <Text>{res.typeSize}</Text>
                </CardItem>
                {Object.entries(res.value).map((_res, _i) => (
                  <CardItem key={_i}>
                    <Body style={{alignSelf: 'center'}}>
                      <Text bold>{`${_res[0]} (cm) :`}</Text>
                    </Body>
                    <Right>
                      <Item regular>
                        <Input 
                          autoCapitalize="none"
                          returnKeyType="next"
                          value={`${_res[1]}`}
                          keyboardType="numeric" 
                          onChangeText={(e) =>  this.onChange(e, i, _res[0]) }
                        />
                      </Item>
                    </Right>
                  </CardItem>
                ))}
              </Card>
            ))}
        </Content>
        <View style={[global.container]}>
          <Button block rounded onPress={() => this.addSize()}>
            <Text>KIRIM</Text>
          </Button>
        </View>
      </NewContainer>
    )
  }
  addSize(){
    const { actionsetEditSize, navigation } = this.props;
    const { _form } = this.state;
    const { params: { uid, onRefresh } } = this.props.route;
    const payload = {
      size: _form,
      uid: uid,
      type: 'size'
    }
    return this.setState({ _loading: true }, () => {
      return actionsetEditSize(payload, () => {
        return this.setState({ _loading: false }, () => {
          Toast.show({ text: "Ukuran berhasil diubah", type: "success"});
          onRefresh();
          return navigation.goBack()
        })
      }, () => {
        return this.setState({ _loading: false }, () => {
          return Toast.show({ text: "Ukuran gagal diubah", type: "danger"});
        })
      })
    })
  }

  componentWillUnmount(){
    const { actionunmountListSize } = this.props;
    return actionunmountListSize()
  }
}

const mapActionsToProps = {
  actionsetListSize: setListSize,
  actionsetEditSize: setEditSize,
  actionunmountListSize: unmountListSize
}

const mapStateToProps = (state, props) => {
  return {
    getData: state.project.listSize,
  }
}

export default connect( mapStateToProps, mapActionsToProps)(AddSizeProject);