import React, { Component } from 'react'
import { connect } from 'react-redux';
import { RefreshControl, View } from 'react-native'
import { Container, Content, List, ListItem, Text, Left, Right, Icon, Toast, Body, Badge } from 'native-base';
import { Header, Loading, NewContainer } from '../../components';
import { setListProject, setLoadMoreListProject, unmountListProject } from '../../../redux/actions/project/projectAction';
import moment from 'moment';

class ListProject extends Component {
  constructor(props) {
    super(props)
    this.state = {
      _current: 1,
      _filter: {
        _type: 'created_at',
        _orderBy: 'desc',
        _search: null,
      },
      _loading: false
    }
  }
  
  componentDidMount(){
    return this.getData();
  }

  getData() {
    const { actionsetListProject } = this.props;
    const { _current, _filter } = this.state;
    const payload = {
      current: _current,
      filter: _filter
    }
    return actionsetListProject(payload);
  }

  onRefresh(){
    return this.setState({
      _current: 1,
      _filter: {
        _type: 'created_at',
        _orderBy: 'desc',
        _search: null,
      },
    }, () => this.getData())
  }

  setCurrentReadOffset = (event) => {
    const { actionsetLoadMoreListProject } = this.props;
    let scrollHeight = event.nativeEvent.contentOffset.y + event.nativeEvent.layoutMeasurement.height;
    if(scrollHeight == event.nativeEvent.contentSize.height){
      return this.setState({ _current: this.state._current + 1, _loading: true }, () => {
        var value = { 
          current: this.state._current,
          filter: this.state._filter
        }
        return actionsetLoadMoreListProject(value, () => {
          this.setState({_loading: false})
        });
      })      
    }
  }

  render() {
    const { getData: {data, loading }, navigation } = this.props;
    const { _loading } = this.state;
    if(loading){
      return <Loading />
    }
    return (
      <NewContainer loading={_loading}>
        <Header show={true} title="List Project"/> 
        <Content 
          refreshControl={<RefreshControl onRefresh={() => this.onRefresh()} />}
          scrollEventThrottle={300}
          onScroll={this.setCurrentReadOffset}
        >
          <List>
            {data.map(( res, i ) => {
              const type = res.count === res.total ? true : false;
              return(
                <ListItem onPress={() => navigation.navigate('ProjectDetailPage', { uid: res.uid })} button key={i}>
                  <Left>
                    <View style={{flexDirection: 'column'}}>
                      <Text title style={{alignSelf: 'flex-start'}}>{res.name}</Text>
                      <Text note style={{fontSize: 12}}>Deadline: {moment(res.deadline).format('DD MMMM YYYY')}</Text>
                    </View>
                  </Left>
                  <Right>

                    <View style={{flexDirection: 'row'}}>
                      <View style={{flexDirection: 'row'}}>
                        <Badge success={type} style={{flexDirection: 'row', alignItems: 'center', marginTop: -3}}>
                          <Text note style={{fontSize: 12, color: '#fff'}}>{res.count} /</Text>
                          <Text note style={{fontSize: 12, color: '#fff', marginLeft: -4}}>{res.total}</Text>
                        </Badge>
                      </View>
                      <Icon name="right" type="AntDesign" style={{alignSelf: 'center', marginLeft: 10}}/>
                    </View>
                  </Right>
                </ListItem>
              )
            })}
          </List>
        </Content>
      </NewContainer>
    )
  }

  componentWillUnmount(){
    const { actionunmountListProject } = this.props;
    return actionunmountListProject();
  }
}


const mapActionsToProps = {
  actionsetListProject: setListProject,
  actionsetLoadMoreListProject : setLoadMoreListProject,
  actionunmountListProject: unmountListProject,
}

const mapStateToProps = (state, props) => {
  return {
    getData: state.project.list,
  }
}

export default connect( mapStateToProps, mapActionsToProps)(ListProject);