import React, { Component } from 'react'
import { connect } from 'react-redux';
import { RefreshControl, View } from 'react-native'
import { Container, Content, List, ListItem, Text, Left, Right, Icon, Toast, Body, Badge } from 'native-base';
import { Header, Loading } from '../../components';
import { setListProject, setListProjectMore, unmountListProject } from '../../../redux/actions/project/projectAction';
import moment from 'moment';

class ListProject extends Component {
  constructor(props) {
    super(props)
    this.state = {
      _current: 1,
      _type: 'created_at',
      _orderBy: 'desc',
      _loading: false
    }
  }
  
  componentDidMount(){
    return this.getData();
  }

  getData() {
    const { actionsetListProject } = this.props;
    const { _current, _type, _orderBy } = this.state;
    const payload = {
      current: _current,
      type: _type,
      orderBy: _orderBy,
    }
    return actionsetListProject(payload);
  }

  onRefresh(){
    this.setState({ _loading: true, _current: 0 }, () => {
      this.setState({ _loading: false })
      return this.getData();
    })
  }

  setCurrentReadOffset = (event) => {
    const { actionsetListProjectMore } = this.props;
    let scrollHeight = event.nativeEvent.contentOffset.y + event.nativeEvent.layoutMeasurement.height;
    if(scrollHeight == event.nativeEvent.contentSize.height){
      return this.setState({ _current: this.state._current + 1, _loading: true }, () => {
        var value = { 
          current: this.state._current,
          type: this.state._type,
          orderBy: this.state._orderBy,
        }
        return actionsetListProjectMore(value, () => {
          this.setState({_loading: false})
        });
      })      
    }
  }

  render() {
    const { getData: {data, loading }, navigation } = this.props;
    if(loading){
      return <Loading />
    }
    return (
      <Container>
        <Header show={true} title="List Project"/> 
        <Content 
          refreshControl={<RefreshControl onRefresh={() => this.onRefresh()} />}
          scrollEventThrottle={300}
          onScroll={this.setCurrentReadOffset}
        >
          <List>
            {data.map(( res, i ) => {
              const type = res.count === res.total ? true : false;
              return(
                <ListItem onPress={() => navigation.navigate('ProjectDetailPage', { data: res })} button key={i}>
                  <Left>
                    <View style={{flexDirection: 'column'}}>
                      <Text title style={{alignSelf: 'flex-start'}}>{res.name}</Text>
                      <Text note style={{fontSize: 12}}>Deadline: {moment(res.deadline).format('DD MMMM YYYY')}</Text>
                    </View>
                  </Left>
                  <Right>
                    <Text bold style={{fontSize: 12, marginBottom: 5, marginRight: 8}}>Total item:</Text>
                    <View style={{flexDirection: 'row'}}>
                      <Badge success={type} style={{flexDirection: 'row', alignItems: 'center', marginTop: -3}}>
                        <Text note style={{fontSize: 12, color: '#fff'}}>{res.count} /</Text>
                        <Text note style={{fontSize: 12, color: '#fff', marginLeft: -4}}>{res.total}</Text>
                      </Badge>
                      {/* <Icon name="right" type="AntDesign" /> */}
                    </View>
                  </Right>
                </ListItem>
              )
            })}
          </List>
        </Content>
      </Container>
    )
  }

  componentWillUnmount(){
    const { actionunmountListProject } = this.props;
    return actionunmountListProject();
  }
}


const mapActionsToProps = {
  actionsetListProject: setListProject,
  actionsetListProjectMore : setListProjectMore,
  actionunmountListProject: unmountListProject,
}

const mapStateToProps = (state, props) => {
  return {
    getData: state.project.list,
  }
}

export default connect( mapStateToProps, mapActionsToProps)(ListProject);