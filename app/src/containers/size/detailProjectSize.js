import React, { Component } from 'react'
import { connect } from 'react-redux';
import { View, RefreshControl, Dimensions } from 'react-native'
import { Container, Content, List, ListItem, Text, Left, Right, Icon, Body } from 'native-base';
import { Header, Loading, NewContainer } from '../../components';
import { setDetailProject, setLoadMoreDetailProject, unmountDetailProject } from '../../../redux/actions/project/projectAction';

class DetailProject extends Component {
  constructor(props) {
    super(props)
    this.state = {
      _current: 1,
      _filer: {
        _type: 'created_at',
        _orderBy: 'desc',
        _search: null,
      },
      _loading: false
    }
  }
  
  componentDidMount(){
    return this.getData();
  }

  getData(){
    const { actionsetDetailProject } = this.props;
    const { params: { data } } = this.props.route;
    const { _current, _filer } = this.state;
    var payload = {
      uid: data.uid,
      current: _current,
      filter: _filer
    }
    return actionsetDetailProject(payload);
  }
  
  onRefresh(){
    return this.setState({
      _current: 1,
      _filer: {
        _type: 'created_at',
        _orderBy: 'desc',
        _search: null,
      },
    }, () => this.getData())
  }

  setCurrentReadOffset = (event) => {
    const { actionsetLoadMoreDetailProject } = this.props;
    const { params: { data } } = this.props.route;
    const scrollHeight = event.nativeEvent.contentOffset.y + event.nativeEvent.layoutMeasurement.height;
    if(scrollHeight == event.nativeEvent.contentSize.height){
      return this.setState({ _current: this.state._current + 1, _loading: true }, () => {
        var value = { 
          uid: data.uid,
          current: this.state._current,
          filter: this.state._filer
        }
        return actionsetLoadMoreDetailProject(value, () => {
          this.setState({_loading: false})
        });
      })
      
    }
  }

  render() {
    const { getData: { data, loading }, navigation } = this.props;
    const { _loading } = this.state;
    if(loading){
      return <Loading />
    }
    return (
      <NewContainer loading={_loading}>
        <Content 
          refreshControl={<RefreshControl onRefresh={() => this.onRefresh()} />}
          scrollEventThrottle={300}
          onScroll={this.setCurrentReadOffset}
        >
          <List>
            {data.map((res, i) => (
              <ListItem key={i} onPress={() => console.log('ss')}>
                <Body>
                  <View style={{marginBottom: 10}}>
                    <Text note bold>Nama:</Text>
                    <Text note>{res.name}</Text>
                  </View>
                  <View style={{marginBottom: 10}}>
                    <Text note bold>Sudah Diukur:</Text>
                    <Text note danger={!res.statusSize}>{res.statusSize ? 'Sudah' : 'Belum Di Ukur'}</Text>
                  </View>
                </Body>
                <Right>
                  <Icon name="arrow-forward" />
                </Right>
              </ListItem>
            ))}
          </List>
        </Content>
      </NewContainer>
    )
  }

  componentWillUnmount(){
    const { acctionunmountDetailProject } = this.props;
    return acctionunmountDetailProject();
  }
}


const mapActionsToProps = {
  actionsetDetailProject: setDetailProject,
  actionsetLoadMoreDetailProject: setLoadMoreDetailProject,
  acctionunmountDetailProject: unmountDetailProject,
}

const mapStateToProps = (state, props) => {
  return {
    getData: state.project.detail,
  }
}

export default connect( mapStateToProps, mapActionsToProps)(DetailProject);