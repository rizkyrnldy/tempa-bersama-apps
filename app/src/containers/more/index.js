import React, { Component } from 'react'
import { Alert } from 'react-native';
import { Container, Content, Separator, List, ListItem, Text, Icon, Left, Body, Right, Switch } from 'native-base';
import { Header } from '../../components';
import { connect } from 'react-redux'
import { TouchableNativeFeedback } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';
import { setLogout } from '../../../redux/actions/authAction';

export class index extends Component {
  
  render() {
    const { navigation } = this.props;
    return (
      <Container>
        <Header show={true} title="Lainnya"/>
        <Content style={{backgroundColor: '#fff'}}>
          <List>
            <ListItem button onPress={() => this.onLogout()}>
              <Left>
                <Text>Keluar</Text>
              </Left>
              <Right>
                <Icon name="ios-arrow-forward" />
              </Right>
            </ListItem>
          </List>
          <Text style={{textAlign: 'center', marginTop: 20, fontSize: 12}}>Tempa V.0.1</Text>
        </Content>
      </Container>
    )
  }

  onLogout(){
    Alert.alert( "Apakah anda yakin keluar?", "",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => this.onLogoutFunction() }
      ],
      { cancelable: false }
    );
  }
  onLogoutFunction(){
    const { actionReqLogout } = this.props;
    return actionReqLogout(() => AsyncStorage.removeItem('@loginUser:key'), () => AsyncStorage.removeItem('@loginUser:key'))
  }

}

const mapStateToProps = (state) => ({
  
})

const mapDispatchToProps = {
  actionReqLogout: setLogout 
}

export default connect(mapStateToProps, mapDispatchToProps)(index)
