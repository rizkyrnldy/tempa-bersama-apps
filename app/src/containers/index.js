import Login from './login/Login';


// Tabbar
import Project from './project/listProject';
import More from './more';
import Scanner from './scanner';

// PAGES
import ProjectDetail from './project/detailProject';
import ListProjectItem from './project/listProjectItem';
import AddSizeProject from './project/addSizeProject';

import ScannerDetail from './scanner/DetailScanner';


export{
    Login,
    Project,
    More,
    Scanner,
    
    ProjectDetail,
    ListProjectItem,
    AddSizeProject,

    ScannerDetail,

}