import React, { Component } from 'react'
import { connect } from 'react-redux'
import { ToastAndroid, StyleSheet, Modal, View, Image, } from 'react-native'
import { Container, Content, Button, Text, Form, Item, Input, Label, Toast } from 'native-base';
import { Header } from '../../components';
import { reqLogin } from '../../../redux/actions/authAction';

class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
       userName: 'rizky',
       password: 'pass123',
    }
  }
  
  render() {
    const { userName, password } = this.state;

    return (
      <Container>
        <Content padder contentContainerStyle={{flex: 1, justifyContent: 'center'}}>
          <View style={{marginBottom: 40, alignItems: 'center'}}>
            <Image 
              source={require('../../img/logo-blue.png')}
              style={{width: 250, height: 27}}
            />
          </View>
          <Form style={{marginBottom: 20}}>
            <Item stackedLabel>
              <Label>Username</Label>
              <Input 
                // autoFocus = {true}
                placeholder="Your Username"
                autoCapitalize="none"
                returnKeyType="next"
                value={userName}
                onChangeText={(e) => this.setState({ userName: e})}
                onSubmitEditing={(event) => {
                  this.lastNameRef._root.focus();
                }}
              />
            </Item>
            <Item stackedLabel>
              <Label>Password</Label>
              <Input 
                ref={(input) =>  this.lastNameRef = input}
                placeholder="Password" 
                autoCapitalize="none"
                secureTextEntry={true} 
                returnKeyType="done"
                value={password}
                onChangeText={(e) => this.setState({ password: e})}
                onSubmitEditing={() => this.onSubmit()}
              />
            </Item>
          </Form>
          <Button full rounded onPress={() => this.onSubmit()}>
            <Text>MASUK</Text>
          </Button>
        </Content>
      </Container>
    )
  }

  onSubmit(){
    const { actionreqLogin } = this.props;
    const { userName, password } = this.state;
    var value = { userName, password };
    if(userName && password){
      return actionreqLogin(value, null, (err) => {
        return Toast.show({
          text: err,
          buttonText: "Okay",
          type: "danger"
        });
        // return ToastAndroid.show(err, ToastAndroid.SHORT);
      })
    }else{
      return ToastAndroid.show('Email / Password tidak boleh kosong', ToastAndroid.SHORT);
    }
  }
}

const mapStateToProps = (state) => ({
  
})

const mapDispatchToProps = {
  actionreqLogin: reqLogin  
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
