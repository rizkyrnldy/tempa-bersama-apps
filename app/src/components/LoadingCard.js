import React, { Component } from 'react';
import{ View, StyleSheet, ActivityIndicator, Dimensions, Text } from 'react-native';
import { global } from '../styles'

import ContentLoader, { Rect, Circle } from 'react-content-loader/native'


const {height} = Dimensions.get('window')

export default class LoadingCard extends Component {
  render() {
    return (
      <View style={[global.container, {flex: 1}]}>
        {new Array(3).fill('qty').map((res, i) => {
          return(
            <View style={[global.shadow, global.border,{backgroundColor: '#fff', marginBottom: 10, position: 'relative', borderRadius: 5, overflow: 'hidden', paddingBottom: 0}]} key={i}>
              <ContentLoader 
                height={160}
                width={400}
                speed={2}
                primaryColor="#f3f3f3"
                secondaryColor="#cac9cb"
              >
                <Rect x="0" y="0" rx="0" ry="0" width="400" height="81" /> 
                <Circle cx="32" cy="120" r="25" /> 
                <Rect x="66" y="98" rx="3" ry="3" width="200" height="6" /> 
                <Rect x="66" y="117" rx="3" ry="3" width="150" height="6" /> 
                <Rect x="65" y="137" rx="3" ry="3" width="200" height="6" />
              </ContentLoader>
            </View>
          )
        })}
      </View>
    );
  }
}



const _styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: height - 118,
    // backgroundColor: '#fff',

  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
})

