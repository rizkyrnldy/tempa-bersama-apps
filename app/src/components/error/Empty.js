import React, { Component } from 'react'
import { View, Image } from 'react-native';
import { Container, Content, Text, Header, List, ListItem, Item, Icon, Input, Left, Body, Right, Title, Button,} from 'native-base';
import { global } from '../../styles';

export default class Empty extends Component {
  render() {
    return (
      <Container>
        <View style={[global.centered, {marginTop: -50}]}>
          <Text>CARI GERAI TERDEKAT ANDA</Text>
        </View>
      </Container>
    )
  }
}
