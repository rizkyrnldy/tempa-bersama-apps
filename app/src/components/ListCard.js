import React, { Component } from 'react';
import{ View, Text, TouchableNativeFeedback, Image } from 'react-native';
import { global } from '../styles';
import NumberFormat from 'react-number-format';
import { Actions } from 'react-native-router-flux';

export default class ListCard extends Component {
  render() {
    const { item } = this.props;
    return (
      <TouchableNativeFeedback onPress={() => Actions.detailStore({dataStore: item, vendor_id: item.id, title: item.name})}>
        <View style={[global.shadow, global.border,{backgroundColor: '#fff', marginBottom: 10, position: 'relative', borderRadius: 5, overflow: 'hidden', paddingBottom: 0}]}>
          <View style={{backgroundColor: '#000'}}>
            <Image 
              source={item.logo ? {uri: item.logo } : require('../img/img-default.jpg')}
              style={{width: null, height: 130, opacity: 0.5}}
            />
          </View>
          
          <View style={{position: 'absolute', right: -20, top: -20, backgroundColor: '#E64E4E', paddingTop: 25, paddingLeft: 20, paddingRight: 30, paddingBottom: 8, borderRadius: 15, zIndex: 1}}>
            <Text style={[global.fontRegular, {fontSize: 10, textAlign: 'right', color: '#fff', textDecorationLine: 'line-through'}]}>
              OPEN NOW
            </Text>
            <Text style={[global.fontBold, {fontSize: 12, textAlign: 'right', color: '#fff'}]}>
              10:00 - 20:00
            </Text>
          </View>
          
          <View style={{  padding: 10, paddingBottom: 0, paddingHorizontal: 10, flexWrap: 'wrap', flexDirection: 'row', position: 'relative',}}>
            <View style={{flex: 0, marginRight: 10, position: 'relative', top: -40}}>
              <Image 
                source={item.logo ? {uri: item.logo } : require('../img/img-default.jpg')}
                style={{width: 60, height: 60, borderRadius: 100}}
              />
            </View>
            <View style={{flex: 1}}>
              <View>
                <Text style={[global.fontBold, {fontSize: 16, color: '#3e3e3e'}]} numberOfLines={1}>
                  {item.name}
                </Text>
              </View>
              
              <View style={[{position: 'relative', marginTop: 5, flexDirection: 'column'}]}>
                <View style={{flexDirection: 'row'}}>
                  <Image 
                    source={require('../img/ic-marker-map.png')}
                    style={{width: 15, height: 15, marginRight: 5, marginTop: 1}}
                  />
                   <NumberFormat 
                    value={item.newDistance} 
                    displayType={'text'}
                    thousandSeparator="."
                    decimalSeparator=","
                    fixedDecimalScale={true}
                    decimalScale={1}
                    isNumericString={true}
                    renderText={ value => (                      
                      <Text style={[global.fontBold, {color: '#3e3e3e', fontSize: 12}]}>
                        {value} KM
                      </Text>
                    )} 
                  />
                </View>
              </View>
            </View>
          </View>

        </View>
      </TouchableNativeFeedback>
    );
  }
}