import Loading from './Loading';
import Header from './Header';
import NotFound from './error/NotFound';
import Empty from './error/Empty';
// import Textinput from './Textinput-del';
import Radio from './Radio';
import NewContainer from './NewContainer';

export{
    Loading,
    Header,
    NotFound,
    Empty,
    // Textinput,
    Radio,
    NewContainer
}