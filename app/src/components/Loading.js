import React, { Component } from 'react';
import{ View, ActivityIndicator } from 'react-native';
import { global } from '../styles'
import { Text } from 'native-base';

export default class Loading extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <View style={[global.centered]}>
          <ActivityIndicator
            style={[{ marginBottom: 10 }]}
            size="large"
            color="#0d47a1"
          />
          <Text style={{color: '#0d47a1'}}>Loading</Text>
        </View>
      </View>
    );
  }
}
