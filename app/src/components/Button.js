import React, { Component } from 'react';
import{ View, Text, TouchableNativeFeedback, ActivityIndicator} from 'react-native';
import { global } from '../styles';

export default class Button extends Component {
  render() {
    const {title, color, onPress, loading, style} = this.props;
    if(loading){
      return(
        <View style={[style, global.bgGold, {padding: 10, borderRadius: 10}]}>
          <Text style={{fontSize: 14, textAlign:'center', color: '#3e3e3e', fontWeight: 'bold'}}>
            {title}
          </Text>
          <ActivityIndicator
            style={[{ height: 20, position: 'absolute', right: 20, top: 10 }]}
            size="small"
            color="#3e3e3e"
          />
        </View>
      )
    }
    return (
      <View>
        <TouchableNativeFeedback onPress={() => onPress()}>
          <View style={[style, global.bgGold, {padding: 10, borderRadius: 10 }]}>
            <Text style={{fontSize: 14, textAlign:'center', color: '#352228', fontWeight: 'bold'}}>
              {title}
            </Text>
          </View>
        </TouchableNativeFeedback>
      </View>
    );
  }
}