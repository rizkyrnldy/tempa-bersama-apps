import React, { Component } from 'react'
import { ToastAndroid, View } from 'react-native';
import { Autocomplete } from './index';
import { Input, Item, Label, Picker, Icon, DatePicker } from 'native-base';
import moment from 'moment';
// import { global } from '../styles';

export default class Textinput extends Component {
  render() {
    const { data } = this.props;
    return(
      data.map((res, i) => {
        switch (res.type) {
          case 'autocomplete':
            return this.renderAutoComplete(res, i);
          case 'picker':
            return this.renderPicker(res, i);
          case 'datePicker':
            return this.renderDatePicker(res, i);
          default:
            return this.renderInput(res, i);
        }
      })
    )
  }

  renderInput(res, i){
    const { onChangeText } = this.props;
    return(
      <View style={{marginBottom: 20}} key={i}>
        <Label style={{marginBottom: 10, fontSize: 14, fontWeight: 'bold'}}>{res.title}:</Label>
        <Item error={res.error} regular>
          <Input 
            // ref={(c) => this._fullName_ = c}
            returnKeyType="next" 
            placeholderTextColor="#ddd"
            value={res.value}
            placeholder={res.title}
            keyboardType={res.keyboardType ? res.keyboardType : 'default'}
            autoCapitalize={res.capital ? res.capital : 'none'}
            multiline={res.type === 'textArea' ? true : false}
            numberOfLines={res.type === 'textArea' ? 6 : 1}
            textAlignVertical={res.type === 'textArea' ? "top" : "center"}
            style={{paddingTop: res.type === 'textArea' ? 10 : 0}}
            onChangeText={(e) => onChangeText(e, i)}
            // onSubmitEditing={() => this._gender_._root.focus() }
          />
        </Item>
      </View>
    )
  }

  renderDatePicker(res, i){
    const { onChangeText } = this.props;
    console.log(22, );
    return(
      <View style={{marginBottom: 20}} key={i}>
        <Label style={{marginBottom: 10, fontSize: 14, fontWeight: 'bold'}}>{res.title}:</Label>
        <Item error={res.error} regular>
          <DatePicker
            defaultDate={res.value ? moment(res.value).toDate() : new Date(2002, 1, 1)}
            // defaultDate={Date.now()}
            maximumDate={new Date(2002, 1, 1)}
            locale={"id"}
            modalTransparent={true}
            animationType={"fade"}
            androidMode={"spinner"}
            textStyle={{ color: "#707070" }}
            placeHolderText={res.value ? moment(res.value).format('D/M/YYYY') : null}
            onDateChange={(e) => onChangeText(moment(e).format('YYYY-MM-DD'), i)}
            style={{backgroundColor: 'red'}}
          />
        </Item>
      </View>
    )
  }

  renderAutoComplete(res, i){
    const { onChangeText } = this.props;
    return(
      <View style={{marginBottom: 20}} key={i}>
        <Autocomplete 
          label={res.title}
          value={res.value.label}
          onChange={(e) => onChangeText(e, i)}
          error={res.error}
        />
      </View>
    )
  }

  renderPicker(res, i){
    const { onChangeText } = this.props;
    return(
      <View style={{marginBottom: 20}} key={i}>
        <Label style={{marginBottom: 10, fontSize: 14, fontWeight: 'bold'}}>{res.title}:</Label>
          <Item error={res.error} regular>
            <Picker
              mode="dropdown"
              iosIcon={<Icon name="arrow-down" />}
              style={{ width: '100%' }}
              placeholder="Jenis Kelamin"
              placeholderStyle={{ color: "#bfc6ea" }}
              placeholderIconColor="#007aff"
              selectedValue={res.value}
              onValueChange={(e) => onChangeText(e, i)}
            >
              <Picker.Item label={res.title}/>
              <Picker.Item label="Laki-Laki" value="Male" />
              <Picker.Item label="Perempuan" value="Female" />
            </Picker>
          </Item>
      </View>
    )
  }

  
}
