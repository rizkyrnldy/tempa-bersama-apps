import React, { Component } from 'react'
import { Text, View } from 'react-native'

export default class Radio extends Component {
  render() {
    const { active } = this.props;
    return (
      <View 
        style={{width: 15, height: 15, backgroundColor: active ? '#0d47a1' : 'transparent', borderWidth: 2, borderColor: '#333' , borderRadius: 100}}
      />
    )
  }
}
