const SET_DETAIL_PROJECT_SIZE = 'SET_DETAIL_PROJECT_SIZE';
const SET_DETAIL_PROJECT_SIZE_SUCCESS = 'SET_DETAIL_PROJECT_SIZE_SUCCESS';
const SET_DETAIL_PROJECT_SIZE_FAILED = 'SET_DETAIL_PROJECT_SIZE_FAILED';
const UNMOUNT_DETAIL_PROJECT_SIZE = 'UNMOUNT_DETAIL_PROJECT_SIZE';

const initialState = {
  loading: true,
  data: null
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_DETAIL_PROJECT_SIZE:
      return {
        ...state,
        loading: true,
      };
    case SET_DETAIL_PROJECT_SIZE_SUCCESS:
      return {
        ...state,
        loading: false,
        data: payload.data,
      };
    case SET_DETAIL_PROJECT_SIZE_FAILED:
      return {
        ...state,
        loading: true,
        data: null
      };
    case UNMOUNT_DETAIL_PROJECT_SIZE:
      return {
        ...state,
        loading: true,
        data: null
      };
    default:
      return state;
  }
}
