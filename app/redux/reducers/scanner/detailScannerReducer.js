const SET_DETAIL_SCANNER = 'SET_DETAIL_SCANNER';
const SET_DETAIL_SCANNER_SUCCESS = 'SET_DETAIL_SCANNER_SUCCESS';
const SET_DETAIL_SCANNER_FAILED = 'SET_DETAIL_SCANNER_FAILED';
const UNMOUNT_DETAIL_SCANNER = 'UNMOUNT_DETAIL_SCANNER';

const initialState = {
  loading: true,
  data: null
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_DETAIL_SCANNER:
      return {
        ...state,
        loading: true,
      };
    case SET_DETAIL_SCANNER_SUCCESS:
      return {
        ...state,
        loading: false,
        data: payload.data,
      };
    case SET_DETAIL_SCANNER_FAILED:
      return {
        ...state,
        loading: true,
        data: null
      };
    case UNMOUNT_DETAIL_SCANNER:
      return {
        ...state,
        loading: true,
        data: null
      };
    default:
      return state;
  }
}
