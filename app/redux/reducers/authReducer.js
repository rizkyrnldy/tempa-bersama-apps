const LOAD_AUTH = 'LOAD_AUTH';
const LOAD_AUTH_LOADING = 'LOAD_AUTH_LOADING';
const LOAD_AUTH_SUCCESS = 'LOAD_AUTH_SUCCESS';
const LOAD_AUTH_FAILED = 'LOAD_AUTH_FAILED';

const LOGOUT = 'LOGOUT';
const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
const LOGOUT_FAILED = 'LOGOUT_FAILED';

const initialState = {
  loading: true,
  authed: false,
  dataUser: null,
  token: null,
  refreshToken: null,
};

export default function auth(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD_AUTH:
      return {
        ...state,
        loading: true,
      };
    case LOAD_AUTH_SUCCESS:
      return {
        ...state,
        loading: false,
        authed: action.payload.authed,
        dataUser: action.payload.dataUser,
        token: action.payload.token,
        refreshToken: action.payload.refreshToken,
      };
    case LOAD_AUTH_FAILED:
      return {
        ...state,
        loading: false,
        authed: false
      };
    case LOAD_AUTH_LOADING:
      return {
        ...state,
        loading: true,
      };
      
    case LOGOUT:
      return {
        ...state,
        loading: true,
      };
      
    case LOGOUT_SUCCESS:
      return {
        ...state,
        loading: false,
        authed: false,
        dataUser: null,
        token: null,
        refreshToken: null
      };
    case LOGOUT_FAILED:
      return {
        ...state,
        loading: false,

        authed: false,
        dataUser: null,
        token: null,
        refreshToken: null,
      };

    default:
      return state;
  }
}
