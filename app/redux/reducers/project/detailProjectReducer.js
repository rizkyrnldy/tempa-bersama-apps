const SET_DETAIL_PROJECT = 'SET_DETAIL_PROJECT';
const SET_DETAIL_PROJECT_SUCCESS = 'SET_DETAIL_PROJECT_SUCCESS';
const SET_DETAIL_PROJECT_FAILED = 'SET_DETAIL_PROJECT_FAILED';
const UNMOUNT_DETAIL_PROJECT = 'UNMOUNT_DETAIL_PROJECT';

const initialState = {
  loading: true,
  data: null
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_DETAIL_PROJECT:
      return {
        ...state,
        loading: true,
      };
    case SET_DETAIL_PROJECT_SUCCESS:
      return {
        ...state,
        loading: false,
        data: payload.data,
      };
    case SET_DETAIL_PROJECT_FAILED:
      return {
        ...state,
        loading: true,
        data: null
      };
    case UNMOUNT_DETAIL_PROJECT:
      return {
        ...state,
        loading: true,
        data: null
      };
    default:
      return state;
  }
}
