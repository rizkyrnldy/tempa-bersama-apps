const SET_LIST_PROJECT = 'SET_LIST_PROJECT';
const SET_LIST_PROJECT_SUCCESS = 'SET_LIST_PROJECT_SUCCESS';
const SET_LIST_PROJECT_FAILED = 'SET_LIST_PROJECT_FAILED';
const UNMOUNT_LIST_PROJECT = 'UNMOUNT_LIST_PROJECT';

const initialState = {
  loading: true,
  data: []
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_LIST_PROJECT:
      return {
        ...state,
        loading: true,
      };
    case SET_LIST_PROJECT_SUCCESS:
      return {
        ...state,
        loading: false,
        data: payload.data,
      };
    case SET_LIST_PROJECT_FAILED:
      return {
        ...state,
        loading: true,
        data: []
      };
    case UNMOUNT_LIST_PROJECT:
      return {
        ...state,
        loading: true,
        data: []
      };
    default:
      return state;
  }
}
