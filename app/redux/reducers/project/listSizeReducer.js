const SET_LIST_SIZE = 'SET_LIST_SIZE';
const SET_LIST_SIZE_SUCCESS = 'SET_LIST_SIZE_SUCCESS';
const SET_LIST_SIZE_FAILED = 'SET_LIST_SIZE_FAILED';
const UNMOUNT_LIST_SIZE = 'UNMOUNT_LIST_SIZE';

const initialState = {
  loading: true,
  data: []
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_LIST_SIZE:
      return {
        ...state,
        loading: true,
      };
    case SET_LIST_SIZE_SUCCESS:
      return {
        ...state,
        loading: false,
        data: payload.data,
      };
    case SET_LIST_SIZE_FAILED:
      return {
        ...state,
        loading: true,
        data: []
      };
    case UNMOUNT_LIST_SIZE:
      return {
        ...state,
        loading: true,
        data: []
      };
    default:
      return state;
  }
}
