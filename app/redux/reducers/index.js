import { combineReducers } from 'redux';
import auth from './authReducer';
import listProject from './project/listProjectReducer';
import detailProject from './project/detailProjectReducer';
import listProjectItem from './project/listProjectItemReducer';
import listSize from './project/listSizeReducer';

import detailScanner from './scanner/detailScannerReducer';


export default combineReducers({
  auth,
  project: combineReducers({
    list: listProject,
    detail: detailProject,
    listItem: listProjectItem,
    listSize: listSize
  }),
  scanner: detailScanner
});

