import { API } from '../../../config';
import { errorHandler } from '../errorAction';

export const unmountDetailScanner = () => async dispatch => {
  return dispatch({type: 'UNMOUNT_DETAIL_SCANNER'})
}

export const setDetailScanner = (payload, successCB, failedCB) => async dispatch => {
  await dispatch({type: 'SET_DETAIL_SCANNER'});
  return API.GET('/project/detail', payload).then((res)  =>  {
    const body = res.body;
    dispatch({
      type: 'SET_DETAIL_SCANNER_SUCCESS',
      payload: {
        data: body
      }
    });
    return successCB && successCB();
  }).catch((err) => {
    console.log(555, err);
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        (failedCB && failedCB(err), dispatch({ type: 'SET_DETAIL_SCANNER_FAILED' }) ): 
          dispatch(setDetailScanner(payload, successCB, failedCB))
    ));
  });
}

export const setSubmitDetailScanner = (payload, successCB, failedCB) => async dispatch => {
  return API.POST('/project/scan', payload).then(()  =>  {
    return successCB && successCB();
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        failedCB && failedCB(err) : dispatch(setDetailScanner(payload, successCB, failedCB))
    ));
  });
}
