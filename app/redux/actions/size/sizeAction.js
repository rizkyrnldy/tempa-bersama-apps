import { API } from '../../../config';
import { errorHandler } from '../errorAction';

export const unmountListProjectSize = () => async dispatch => {
  return dispatch({type: 'UNMOUNT_LIST_PROJECT'})
}

export const unmountDetailProjectSize = () => async dispatch => {
  return dispatch({type: 'UNMOUNT_DETAIL_PROJECT'})
}

export const setListProjectSize = (payload) => async dispatch => {
  await dispatch({type: 'SET_LIST_PROJECT'});
  return API.GET('/plan/project', payload).then((res)  =>  {
    const body = res.body.data;
    return dispatch({
      type: 'SET_LIST_PROJECT_SUCCESS',
      payload: {
        data: body
      }
    });
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        dispatch({ type: 'SET_LIST_PROJECT_FAILED' }) : 
          dispatch(setListProjectSize(payload))
    ));
  });
}

export const setListProjectSizeMore = (value, successCB) => async (dispatch, getState) => {
  return API.GET('/plan/project', value).then((res) => {
    const body = getState().project.list.data.concat(res.body.data)
    dispatch({ 
      type: 'SET_LIST_PROJECT_SUCCESS', 
      payload: {  
        data: body,
      }
    });
    return successCB && successCB();
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        dispatch({  type: 'SET_LIST_PROJECT_FAILED' }) : 
          dispatch(setListProjectSizeMore(value, successCB))
    ));
  })
}

export const setDetailProjectSize = (payload) => async dispatch => {
  await dispatch({type: 'SET_DETAIL_PROJECT'});
  return API.GET('/project', payload).then((res)  =>  {
    const body = res.body.data;
    return dispatch({
      type: 'SET_DETAIL_PROJECT_SUCCESS',
      payload: {
        data: body
      }
    });
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        dispatch({ type: 'SET_DETAIL_PROJECT_FAILED' }) : 
          dispatch(setDetailProjectSize(payload))
    ));
  });
}


export const setLoadMoreDetailProjectSize = (value, successCB) => async (dispatch, getState) => {
  return API.GET('/project', value).then((res) => {
    const body = getState().project.detail.data.concat(res.body.data)
    dispatch({ 
      type: 'SET_DETAIL_PROJECT_SUCCESS', 
      payload: {  
        data: body,
      }
    });
    return successCB && successCB();
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        dispatch({  type: 'SET_DETAIL_PROJECT_FAILED' }) : 
          dispatch(setDetailProjectSize(value, successCB))
    ));
  })

}

