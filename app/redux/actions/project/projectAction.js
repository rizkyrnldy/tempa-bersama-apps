import { API } from '../../../config';
import { errorHandler } from '../errorAction';

export const unmountListProject = () => async dispatch => {
  return dispatch({type: 'UNMOUNT_LIST_PROJECT'})
}

export const unmountDetailProject = () => async dispatch => {
  return dispatch({type: 'UNMOUNT_DETAIL_PROJECT'})
}
export const unmountListProjectItem = () => async dispatch => {
  return dispatch({type: 'UNMOUNT_LIST_ITEM_PROJECT'})
}
export const unmountListSize = () => async dispatch => {
  return dispatch({type: 'UNMOUNT_LIST_SIZE'})
}

export const setListProject = (payload) => async dispatch => {
  await dispatch({type: 'SET_LIST_PROJECT'});
  return API.GET('/plan/project', payload).then((res)  =>  {
    const body = res.body.data;
    return dispatch({
      type: 'SET_LIST_PROJECT_SUCCESS',
      payload: {
        data: body
      }
    });
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        dispatch({ type: 'SET_LIST_PROJECT_FAILED' }) : 
          dispatch(setListProject(payload))
    ));
  });
}

export const setLoadMoreListProject = (value, successCB) => async (dispatch, getState) => {
  return API.GET('/plan/project', value).then((res) => {
    const body = getState().project.list.data.concat(res.body.data)
    dispatch({ 
      type: 'SET_LIST_PROJECT_SUCCESS', 
      payload: {  
        data: body,
      }
    });
    return successCB && successCB();
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        dispatch({  type: 'SET_LIST_PROJECT_FAILED' }) : 
          dispatch(setLoadMoreListProject(value, successCB))
    ));
  })
}

export const setDetailProject = (payload) => async dispatch => {
  await dispatch({type: 'SET_DETAIL_PROJECT'});
  return API.GET('/plan/project/detail', payload).then((res)  =>  {
    const body = res.body;
    return dispatch({
      type: 'SET_DETAIL_PROJECT_SUCCESS',
      payload: {
        data: body
      }
    });
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        dispatch({ type: 'SET_DETAIL_PROJECT_FAILED' }) : dispatch(setDetailProject(payload))
    ));
  });
}


export const setListProjectItem = (payload, successCB, failedCB) => async dispatch => {
  await dispatch({type: 'SET_LIST_ITEM_PROJECT'});
  return API.GET('/project', payload).then((res)  =>  {
    const body = res.body.data;
    dispatch({
      type: 'SET_LIST_ITEM_PROJECT_SUCCESS',
      payload: {
        data: body
      }
    });
    return successCB && successCB()
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        ( failedCB && failedCB(), dispatch({ type: 'SET_LIST_ITEM_PROJECT_FAILED' }) ) : 
          dispatch(setListProjectItem(payload))
    ));
  });
}


export const setLoadMoreListProjectItem = (value, successCB) => async (dispatch, getState) => {
  return API.GET('/project', value).then((res) => {
    const body = getState().project.listItem.data.concat(res.body.data)
    dispatch({ 
      type: 'SET_LIST_ITEM_PROJECT_SUCCESS', 
      payload: {  
        data: body,
      }
    });
    return successCB && successCB();
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        dispatch({  type: 'SET_LIST_ITEM_PROJECT_FAILED' }) : 
          dispatch(setLoadMoreListProjectItem(value, successCB))
    ));
  })
}

export const setListSize = (payload, successCB, failedCB) => async dispatch => {
  await dispatch({type: 'SET_LIST_SIZE'});
  return API.GET('/project/detail', payload).then((res) => {
    const body = res.body
    dispatch({ 
      type: 'SET_LIST_SIZE_SUCCESS', 
      payload: {  
        data: body,
      }
    });
    return successCB && successCB(body);
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        ( failedCB && failedCB(), dispatch({  type: 'SET_LIST_SIZE_FAILED' }) ) : 
          dispatch(setListSize(payload, successCB, failedCB))
    ));
  })
}
export const setEditSize = (payload, successCB, failedCB) => async dispatch => {
  return API.POST('/project/edit', payload).then((res) => {
    console.log(44, res);
    return successCB && successCB();
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        failedCB && failedCB():  dispatch(setEditSize(payload, successCB, failedCB))
    ));
  })
}


