import AsyncStorage from '@react-native-community/async-storage';
import { API } from '../../config';
import { errorHandler } from './errorAction';
var jwtDecode = require('jwt-decode');

export const checkAuth = () => async dispatch => {
  await dispatch({type: 'LOAD_AUTH'});
  const loginUser = await AsyncStorage.getItem('tokenUser');
  const refreshToken = await AsyncStorage.getItem('refreshTokenUser');
  if(loginUser){
    const dataToken = jwtDecode(loginUser);
    return dispatch({
      type: 'LOAD_AUTH_SUCCESS',
      payload: {
        authed: true,
        dataUser: dataToken.data,
        token: loginUser,
        refreshToken: refreshToken,
      }
    });
  }else{
    return dispatch({ type: 'LOAD_AUTH_FAILED' });
  }
}

export const refreshToken = () => async dispatch => {
  var refreshToken = await AsyncStorage.getItem('refreshTokenUser')
  var payload = { "refreshToken": refreshToken }
  return API.POST('/refresh/token', payload).then((res) => {
    const body = res.body;
    const dataToken = jwtDecode(body.token);
    AsyncStorage.setItem('tokenUser', body.token);
    AsyncStorage.setItem('refreshTokenUser', body.refreshToken);
    return dispatch({ 
      type: 'LOAD_AUTH_SUCCESS', 
      payload: {  
        authed: true,
        dataUser: dataToken.data,
        token: body.token,
        refreshToken: body.refreshToken,
      }
    });
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        ( failedCB && failedCB(err), dispatch({ type: 'LOAD_AUTH_FAILED' }) ): 
          dispatch(refreshToken())
    ));
  })
}

export const reqLogin = (payload, successCB, failedCB) => async dispatch => {
  await dispatch({type: 'LOAD_AUTH'});
  return API.POST('/login', payload).then((res)  =>  {
    const body = res.body;
    const dataToken = jwtDecode(body.token);
    AsyncStorage.setItem('tokenUser', body.token);
    AsyncStorage.setItem('refreshTokenUser', body.refreshToken);
    dispatch({
      type: 'LOAD_AUTH_SUCCESS',
      payload: {
        authed: true,
        dataUser: dataToken.data,
        token: body.token,
        refreshToken: body.refreshToken,
      }
    });
    return successCB && successCB();
  }).catch((err) => {
    dispatch({ type: 'LOAD_AUTH_FAILED' });
    return failedCB && failedCB(err.message);
  });
}

export const setLogout = (successCB, failedCB) => async dispatch => {
  await dispatch({type: 'LOGOUT'});
  AsyncStorage.removeItem('tokenUser');
  AsyncStorage.removeItem('refreshTokenUser');
  return API.GET('/logout').then(()  =>  {
    successCB && successCB()
    return dispatch({ type: 'LOGOUT_SUCCESS' });
  }).catch(() => {
    dispatch({ type: 'LOGOUT_FAILED' });
    return failedCB && failedCB()
  })
}

export const changeToken = (body, cb) => async (dispatch, getState) => {
  if(body.token){
    const dataToken = jwtDecode(body.token);
    AsyncStorage.setItem('tokenUser', body.token);
    AsyncStorage.setItem('refreshTokenUser', body.refreshToken);
    return (
      dispatch({
        type: 'LOAD_AUTH_SUCCESS',
        payload: {
          authed: true,
          dataUser: dataToken.data,
          token: body.token,
          refreshToken: body.refreshToken,
        }
      }),
      cb()
    )
  }else{
    return cb();
  }
}
