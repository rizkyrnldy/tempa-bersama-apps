import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import AllReducer from './reducers';
import { composeWithDevTools } from 'redux-devtools-extension';

const initialState = {};
const middleware = [thunk];

const store = createStore(AllReducer, initialState, composeWithDevTools(applyMiddleware(...middleware)));
export default store;
